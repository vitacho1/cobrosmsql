USE `recuperacionvalores`


CREATE TRIGGER auditoriapago UPDATE/DELETE 
    ON TABLE NAME pago
    FOR EACH ROW BEGIN
	INSERT INT auditoria_pago(`id_codigoPago`,`Fecha`,`justificacion`,`usuario_key`,`valor_anterior`,`valor_actual`,`id_CodigoServicio`,`Usuario`)
	VALUES(old.`CodigoPago`,NOW(),new.justificacion,new.CodigoEmpleado2,old.`MontoTotal`,new.`MontoTotal`,old.`CodigoServicio5`,CURRENT_USER)
	
    END;

DELIMITER ;