#Creacion de la base de datos
#eliminar tabla solo para fase de pruebas
#drop DATABASE RecuperacionValores;
create DATABASE RecuperacionValores;

#Usar la base de datos
USE RecuperacionValores;

#Tablas: Tabla Provincia
CREATE TABLE Provincia (
CodigoUbicacion INT NOT NULL AUTO_INCREMENT,
Nombre VARCHAR(45) NOT NULL,
PRIMARY KEY(CodigoUbicacion)
)ENGINE = INNODB;

#Tablas: Tabla Compañia
CREATE TABLE Compañia (
CodigoCompañia INT NOT NULL AUTO_INCREMENT,
Nombre VARCHAR(45) NOT NULL UNIQUE,
Dirección VARCHAR(45) NOT NULL,
Telefono VARCHAR(10) NOT NULL,
Descripcion TEXT NOT NULL,
Correo VARCHAR(45) NOT NULL,
Logo VARCHAR(45) NOT NULL,
CodigoProvincia INT NULL,
PRIMARY KEY(CodigoCompañia),
INDEX (CodigoProvincia),
FOREIGN KEY (CodigoProvincia) REFERENCES Provincia(CodigoUbicacion)
ON DELETE NO ACTION
ON UPDATE NO ACTION
)ENGINE = INNODB;


#Tablas: Tabla Institucion
CREATE TABLE Institucion (
CodigoInstitucion INT NOT NULL AUTO_INCREMENT,
Nombre VARCHAR(45) NOT NULL,
Direccion VARCHAR(45) NOT NULL,
Telefono VARCHAR(10) NOT NULL,
Correo VARCHAR(45) NOT NULL,
Descripcion TEXT NULL,
PRIMARY KEY (CodigoInstitucion)
)ENGINE = INNODB;

#Tablas: Tabla Convenio
CREATE TABLE Convenio(
CodigoConvenio INT NOT NULL AUTO_INCREMENT,
FechaInicio date  NOT NULL,
Estado TINYINT NULL DEFAULT 1,
Descripcion TEXT NULL,
TipoConvenio VARCHAR(25) NOT NULL,
FechaFin date NOT NULL,
CodigoCompañia1 INT NOT NULL,
CodigoInstitucion1 INT NOT NULL,
PRIMARY KEY (CodigoConvenio),
INDEX (CodigoCompañia1),
FOREIGN KEY (CodigoCompañia1) REFERENCES Compañia(CodigoCompañia)
ON DELETE NO ACTION
ON UPDATE NO ACTION,
INDEX (CodigoInstitucion1),
FOREIGN KEY (CodigoInstitucion1) REFERENCES Institucion(CodigoInstitucion)
ON DELETE NO ACTION
ON UPDATE NO ACTION
)ENGINE = INNODB;

#Tablas: Tabla Usuario
CREATE TABLE Usuario(
CodigoUsuario INT NOT NULL AUTO_INCREMENT,
Nombre VARCHAR(45) NOT NULL,
Apellido VARCHAR(45) NOT NULL,
Cedula VARCHAR(10) NOT NULL UNIQUE,
Direccion VARCHAR(45) NOT NULL,
Telefono VARCHAR(10) NOT NULL UNIQUE,
Correo VARCHAR(25) NOT NULL,
CodigoCompañia2 INT NOT NULL,
PRIMARY KEY (CodigoUsuario),
INDEX (CodigoCompañia2),
FOREIGN KEY (CodigoCompañia2) REFERENCES Compañia(CodigoCompañia)
ON DELETE NO ACTION
ON UPDATE NO ACTION
)ENGINE = INNODB;

#Tablas: Tabla Empleado
CREATE TABLE Empleado(
CodigoEmpleado INT NOT NULL AUTO_INCREMENT,
CodigoIdentificacion INT NOT NULL,
HorasTrabajo INT NOT NULL,
Constraseña varchar(25) not null,
CodigoUsuario1 INT NOT NULL unique,
PRIMARY KEY (CodigoEmpleado),
INDEX (CodigoUsuario1),
FOREIGN KEY (CodigoUsuario1) REFERENCES Usuario(CodigoUsuario)
ON DELETE NO ACTION
ON UPDATE NO ACTION
)ENGINE = INNODB;

#Tablas: Tabla Cliente
CREATE TABLE Cliente(
CodigoCliente INT NOT NULL AUTO_INCREMENT,
RUC VARCHAR(45) NOT NULL,
CodigoUsuario2 INT NOT NULL,
PRIMARY KEY (CodigoCliente),
INDEX (CodigoUsuario2),
FOREIGN KEY (CodigoUsuario2) REFERENCES Usuario(CodigoUsuario)
ON DELETE NO ACTION
ON UPDATE NO ACTION
)ENGINE = INNODB;

#Tablas: Tabla Acceso
CREATE TABLE Acceso(
CodigoAcceso INT NOT NULL AUTO_INCREMENT,
Fecha DATE NULL,
Hora VARCHAR(5) NULL,
CodigoEmpleado1 INT NOT NULL,
PRIMARY KEY (CodigoAcceso),
INDEX (CodigoEmpleado1),
FOREIGN KEY (CodigoEmpleado1) REFERENCES Empleado(CodigoEmpleado)
ON DELETE NO ACTION
ON UPDATE NO ACTION
)ENGINE = INNODB;

#Tablas: Tabla Servicio
CREATE TABLE Servicio(
CodigoServicio INT NOT NULL AUTO_INCREMENT,
NombreServicio VARCHAR(45) NOT NULL,
Descripcion TEXT NULL,
Estado TINYINT NOT NULL DEFAULT 1,
ValorServicio DOUBLE NOT NULL,
justificacion text,
CodigoCliente1 INT NOT NULL,
C_CodigoCompañia INT NOT NULL,
PRIMARY KEY (CodigoServicio),
INDEX (CodigoCliente1),
FOREIGN KEY (CodigoCliente1) REFERENCES Cliente(CodigoCliente)
ON DELETE NO ACTION
ON UPDATE NO ACTION,
INDEX (C_CodigoCompañia),
FOREIGN KEY (C_CodigoCompañia) REFERENCES Compañia(CodigoCompañia)
ON DELETE NO ACTION
ON UPDATE NO ACTION
)ENGINE = INNODB;

#Tablas: Tabla Servicio_Convenio
CREATE TABLE Servicio_Convenio(
idServicio_Convenio INT NOT NULL AUTO_INCREMENT,
CodigoServicio INT NOT NULL,
CodigoConvenio INT NOT NULL,
PRIMARY KEY (idServicio_Convenio),
INDEX (CodigoServicio),
FOREIGN KEY (CodigoServicio) REFERENCES Servicio(CodigoServicio)
ON DELETE NO ACTION
ON UPDATE NO ACTION,
INDEX (CodigoConvenio),
FOREIGN KEY (CodigoConvenio) REFERENCES Convenio(CodigoConvenio)
ON DELETE NO ACTION
ON UPDATE NO ACTION
)ENGINE = INNODB;

#Tablas: Tabla Prestamo
CREATE TABLE Prestamo(
CodigoPrestamo INT NOT NULL AUTO_INCREMENT,
FechaLimite VARCHAR(45) NOT NULL,
TipoPrestamo VARCHAR(45) NOT NULL,
CodigoServicio1 INT NOT NULL,
PRIMARY KEY (CodigoPrestamo),
INDEX (CodigoServicio1),
FOREIGN KEY (CodigoServicio1) REFERENCES Servicio(CodigoServicio)
ON DELETE NO ACTION
ON UPDATE NO ACTION
)ENGINE = INNODB;

#Tablas: Tabla ServicioBasico
CREATE TABLE ServicioBasico(
CodigoSBasico INT NOT NULL AUTO_INCREMENT,
Tipo VARCHAR(45) NOT NULL,
PRIMARY KEY (CodigoSBasico)
)ENGINE = INNODB;

#Tablas: Tabla RegistroServicioB
CREATE TABLE RegistroServicioB(
CodigoRegistroServicioB INT NOT NULL AUTO_INCREMENT,
CodigoSBasico1 INT NOT NULL,
CodigoServicio6 INT NOT NULL,
PRIMARY KEY (CodigoRegistroServicioB),
INDEX (CodigoSBasico1),
FOREIGN KEY (CodigoSBasico1) REFERENCES ServicioBasico(CodigoSBasico)
ON DELETE NO ACTION
ON UPDATE NO ACTION,
INDEX (CodigoServicio6),
FOREIGN KEY (CodigoServicio6) REFERENCES Servicio(CodigoServicio)
ON DELETE NO ACTION
ON UPDATE NO ACTION
)ENGINE = INNODB;

#Tablas: Tabla TelevisionSatelital
CREATE TABLE TelevisionSatelital(
CodigoTSatelital INT NOT NULL AUTO_INCREMENT,
NroDispositivos INT NULL,
Tipo VARCHAR(45) NULL,
CodigoServicio3 INT NOT NULL,
PRIMARY KEY (CodigoTSatelital),
INDEX (CodigoServicio3),
FOREIGN KEY (CodigoServicio3) REFERENCES Servicio(CodigoServicio)
ON DELETE NO ACTION
ON UPDATE NO ACTION
)ENGINE = INNODB;


#Tablas: Tabla Colegiatura
CREATE TABLE Colegiatura(
CodigoColegiatura INT NOT NULL AUTO_INCREMENT,
Año VARCHAR(45) NOT NULL,
NombreEstudiante VARCHAR(45) NOT NULL,
ApellidoEstudiante VARCHAR(45) NOT NULL,
Correo VARCHAR(45) NOT NULL,
InstitucionEducativa VARCHAR(45) NOT NULL,
CodigoServicio4 INT NOT NULL,
PRIMARY KEY (CodigoColegiatura),
INDEX (CodigoServicio4),
FOREIGN KEY (CodigoServicio4) REFERENCES Servicio(CodigoServicio)
ON DELETE NO ACTION
ON UPDATE NO ACTION
)ENGINE = INNODB;

#Tablas: Tabla Pago

CREATE TABLE Pago(
CodigoPago INT NOT NULL AUTO_INCREMENT,
Fecha DATE NOT NULL,
FechaLimite VARCHAR(45) NOT NULL,
MontoTotal DOUBLE NOT NULL,
NroPago VARCHAR(45) NOT NULL,
Estado VARCHAR(45) NOT NULL,
Justificacion text,
CodigoEmpleado2 INT NOT NULL,
CodigoServicio5 INT NOT NULL,
PRIMARY KEY (CodigoPago),
INDEX (CodigoServicio5),
FOREIGN KEY (CodigoServicio5) REFERENCES Servicio(CodigoServicio)
ON DELETE NO ACTION
ON UPDATE NO ACTION,
INDEX (CodigoEmpleado2),
FOREIGN KEY (CodigoEmpleado2) REFERENCES Empleado(CodigoEmpleado)
ON DELETE NO ACTION
ON UPDATE NO ACTION
)ENGINE = INNODB;

#Tablas: Tabla PagoTotal
CREATE TABLE PagoTotal(
CodigoPagoTotal INT NOT NULL AUTO_INCREMENT,
MontoRecargo DOUBLE NULL,
CodigoPago1 INT NOT NULL UNIQUE,
PRIMARY KEY (CodigoPagoTotal),
INDEX (CodigoPago1),
FOREIGN KEY (CodigoPago1) REFERENCES Pago(CodigoPago)
ON DELETE NO ACTION
ON UPDATE NO ACTION
)ENGINE = INNODB;

#Tablas: Tabla PagoParcial
CREATE TABLE PagoParcial(
CodigoPagoParcial INT NOT NULL AUTO_INCREMENT,
MontoRecargoParcial DOUBLE NOT NULL,
PorcentajePago VARCHAR(45) NULL,
NroCuotas INT NULL DEFAULT 0,
CodigoPago2 INT NOT NULL,
PRIMARY KEY (CodigoPagoParcial),
INDEX (CodigoPago2),
FOREIGN KEY (CodigoPago2) REFERENCES Pago(CodigoPago)
ON DELETE NO ACTION
ON UPDATE NO ACTION
)ENGINE = INNODB;

#Tablas: Tabla Cuota
CREATE TABLE Cuota(
CodigoCuota INT NOT NULL AUTO_INCREMENT,
NroCuota INT NOT NULL,
NroCuotasPagadas INT NOT NULL,
ValorCuota DOUBLE NOT NULL,
ValorTotal DOUBLE NOT NULL,
Estado TINYINT NULL DEFAULT 1,
CodigoPagoP1 INT NOT NULL,
PRIMARY KEY (CodigoCuota),
INDEX (CodigoPagoP1),
FOREIGN KEY (CodigoPagoP1) REFERENCES PagoParcial(CodigoPagoParcial)
ON DELETE NO ACTION
ON UPDATE NO ACTION
)ENGINE = INNODB;

#Tablas: Tabla Comprobante
CREATE TABLE Comprobante(
CodigoComprobante INT NOT NULL AUTO_INCREMENT,
MontoCancelado DOUBLE NOT NULL,
Fecha DATE NOT NULL,
Estado VARCHAR(45) NULL,
CodigoPago3 INT NOT NULL,
PRIMARY KEY (CodigoComprobante),
INDEX (CodigoPago3),
FOREIGN KEY (CodigoPago3) REFERENCES Pago(CodigoPago)
ON DELETE NO ACTION
ON UPDATE NO ACTION
)ENGINE = INNODB;


# tablas de auditoria
create table auditoria_pago(
id_comprobante_auditoria int not null auto_increment primary key,
id_codigoPago int,
Fecha datetime not null,
justificacion text not null,
usuario_key int not null,
valor_anterior double not null,
valor_actual double not null,
id_CodigoServicio int not null,
Usuario char(30) not null,
Accion varchar(20)
);
CREATE TABLE auditoriaservicios (
    idServicios INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    Fecha DATETIME NOT NULL,
    justificacion TEXT NOT NULL,
    valor_anterior DOUBLE NOT NULL,
    valor_actual DOUBLE NOT NULL,
    Usuario char(30) not null,
    accion VARCHAR(20) NOT NULL,
    id_servicio INT NOT NULL
);

create table Historicopagos(
idHistorico int not null auto_increment primary key,
MontoPagda double not null,
CodigoPago int not null,
Fecha datetime not null,
tipo_pago varchar(20) not null
);

create table comprobanteenviar(
idEnviar int not null auto_increment primary key,
CodigoComprobante int not null,
Monto double not null,
Fecha date not null,
Nombre varchar (45),
Correo varchar(25) not null

);
/*
create table Pago_Auditoria(9)
*/




DELIMITER $$
CREATE
    TRIGGER `auditoriapago` BEFORE UPDATE
    ON `recuperacionvalores`.`pago`
    FOR EACH ROW BEGIN
	INSERT auditoria_pago(`id_codigoPago`,`Fecha`,`justificacion`,`usuario_key`,`valor_anterior`,`valor_actual`,`id_CodigoServicio`,`Usuario`,`Accion`)
	VALUES(old.`CodigoPago`,CURRENT_TIMESTAMP,new.justificacion,new.CodigoEmpleado2,old.`MontoTotal`,new.`MontoTotal`,old.`CodigoServicio5`,CURRENT_USER,'Actualizar');
     if (new.Estado = '0') then  	
			/*se procede a crear los datos del comprobante segun los parametros del la tabla pago ya que el estadp cero representa que el pago ya esta canlelado*/
			insert into `recuperacionvalores`.`comprobante` (`MontoCancelado`, `Fecha`, `Estado`,`CodigoPago3`)
			values(new.MontoTotal,current_date(), '0',new.CodigoPago);
	end if;
    
    
    END$$
DELIMITER ;


#trigger para pago eliminacion

DELIMITER $$
CREATE
    
    TRIGGER `auditoriapagoeliminar` BEFORE DELETE
    ON `recuperacionvalores`.`pago`
    FOR EACH ROW BEGIN
	INSERT auditoria_pago(`id_codigoPago`,`Fecha`,`justificacion`,`usuario_key`,
    `valor_anterior`,`valor_actual`,`id_CodigoServicio`,`Usuario`,`Accion`)
	
    VALUES(old.CodigoPago,CURRENT_TIMESTAMP,old.justificacion,old.CodigoEmpleado2,
    old.MontoTotal,'0',old.CodigoServicio5,CURRENT_USER,'Eliminar');
    END$$
DELIMITER ;


/* Triger para el historico de los pago totales*/
DELIMITER $$
CREATE
    TRIGGER `historicototal` AFTER INSERT ON `pagototal` 
    FOR EACH ROW BEGIN
		DECLARE monto_pago DOUBLE; 
		SET monto_pago:= (SELECT MontoTotal FROM pago WHERE CodigoPago = new.CodigoPago1) ; 
		INSERT historicopagos(MontoPagda,CodigoPago,Fecha,tipo_pago)
		VALUES(monto_pago,new.CodigoPago1,CURRENT_TIMESTAMP,'Total') ;
		END;
$$
DELIMITER ;


/*Trigger de PagoParcial */
DELIMITER $$
CREATE
    TRIGGER `historicototalparcial` before INSERT ON `cuota`
    FOR EACH ROW BEGIN
		DECLARE monto_pago DOUBLE; #monto del pago 
        Declare nro_cuota int;
        Declare id_pago int;
		/* Parametros para el calculo de  	*/
		DECLARE valorSer DOUBLE; 
		DECLARE recargoServicio DOUBLE; 
		DECLARE montonew DOUBLE; 
		DECLARE codigoser INT;
        DECLARE montore double;
        
        /* Treaemo el numero de cuentas de la tabla pago parcial*/
        Set nro_cuota := (select NroCuotas FROM pagoparcial where  CodigoPagoParcial = new.CodigoPagoP1 );
        SET id_pago := (select CodigoPago2 From pagoparcial where  CodigoPagoParcial = new.CodigoPagoP1);
        /*sumamos todas cuotas */
         /* traemos el monto del recargo de la tabla pagoparcial*/
		set montore := (select MontoRecargoParcial From pagoparcial where CodigoPagoParcial = new.CodigoPagoP1 );
		/*priemro obtenemos el codigo del servicio de la tabla pago*/
		SET codigoser :=(SELECT CodigoServicio5 FROM Pago WHERE CodigoPago= id_pago);
		/*Con el el codigo obtenedo del servicio se precede a traer el el valor del servicio*/
		SET valorSer := (SELECT ValorServicio FROM servicio WHERE CodigoServicio = codigoser);
		/*pasamos a sumar los dos valores */
		set montonew := valorSer + montore;
		/*guadamos el valor del total */
       
        set new.ValorTotal := montonew;
		set new.ValorCuota := new.ValorTotal  / nro_cuota;
        if (nro_cuota = new.NroCuotasPagadas ) then  	
			/*obtenemos el valor del monto de la couta pagada*/
			/*con la suma del recargo y el valor del servicio se procede a actualizar la tabla pago*/
			update pago set MontoTotal = montonew , Estado = 0 , Justificacion = 'Actualización automatica sumando el monto de recargo del pago total'	
			where CodigoPago = id_pago;
            SET monto_pago:= (SELECT MontoTotal FROM pago WHERE CodigoPago = id_pago); 
			INSERT historicopagos(MontoPagda,CodigoPago,Fecha,tipo_pago)
			VALUES(monto_pago,id_pago,CURRENT_TIMESTAMP,'Parcial');

        end if;
        END;
$$
DELIMITER ;

/*Trigger de Comprobante*/
DELIMITER $$
CREATE
    TRIGGER `Enviarnotificacion` AFTER INSERT ON `comprobante`
    FOR EACH ROW BEGIN
		Declare email varchar(45);
        Declare NombreU varchar (45);
		Declare id int;
        Declare id_servicio int;
        set id_servicio := (SELECT CodigoServicio5 FROM pago where CodigoPago= new.CodigoPago3);
        set id := (select CodigoCliente1 from servicio where CodigoServicio=id_servicio);
        set id := (select CodigoUsuario2 FROM cliente where CodigoCliente=id);
		SET email := (SELECT Correo FROM usuario WHERE CodigoUsuario=id);
        SET NombreU := (SELECT Nombre FROM usuario WHERE CodigoUsuario=id);
		INSERT comprobanteenviar(CodigoComprobante,Monto,Fecha,Correo,nombre)
		values (new.CodigoComprobante,new.MontoCancelado,new.Fecha,email,NombreU);
		END;
$$
DELIMITER ;




/*triger para los servicios */
DELIMITER $$
CREATE
    TRIGGER `Auditoriaserviciosactualizar` AFTER update ON `servicio`
    
    FOR EACH ROW BEGIN
    
	INSERT auditoriaservicios(Fecha,justificacion,valor_anterior,valor_actual,Usuario,accion,id_servicio)
	VALUES(CURRENT_TIMESTAMP,new.justificacion,old.ValorServicio,new.ValorServicio,CURRENT_USER,'Actualizar',
    new.CodigoServicio);
	END;
$$
DELIMITER ;

DELIMITER $$
CREATE
    TRIGGER `Auditoriaservicioseliminar` AFTER delete ON `servicio`
    
    FOR EACH ROW BEGIN
    
	INSERT auditoriaservicios(Fecha,justificacion,valor_anterior,valor_actual,Usuario,accion,id_servicio)
	VALUES(CURRENT_TIMESTAMP,old.justificacion,old.ValorServicio,old.ValorServicio,CURRENT_USER,'Eliminar',
    old.CodigoServicio);
		END;
$$
DELIMITER ;

/*
Procedimineto almacenados 
*/


DELIMITER $$
CREATE
    PROCEDURE `Calculodevalores`(in id_pago int, out resultado double)
	BEGIN
		/*encotrar al cliente */
        declare codusuario int ;
        /*traer el el servicio */
		declare id_servicio int ;
        declare id int;
        Declare num int;
        Declare monto double;
        set id_servicio := (select CodigoServicio5 From pago where CodigoPago =  id_pago);
        set monto := (select MontoTotal from pago where CodigoPago =  id_pago);
		set id := (select CodigoCliente1 from servicio where CodigoServicio=id_servicio);
        set num := (select count(*) from cliente inner join servicio on servicio.CodigoCliente1 = id and cliente.CodigoCliente= id inner join Pago on CodigoServicio5 = servicio.CodigoServicio);
        /*Contamos el resultado de de la busqueda*/
		if (num = '1' )then
			set resultado :='0.01'*monto;
            #set resultado := num;
            elseif(num= '2') then 
			set resultado :='0.05'*monto;
            #set resultado := num;
        else
			set resultado:='0.1'*monto;
            #set resultado := '20';
        end if;
	END$$
DELIMITER ;

/*Triger de pagos */
DELIMITER $$
CREATE
    TRIGGER `pagorecargototal` before insert ON `pagototal`
    FOR EACH ROW BEGIN
		DECLARE valorSer DOUBLE; 
		DECLARE recargoServicio DOUBLE; 
		DECLARE montonew DOUBLE; 
		DECLARE codigoser INT;
        
        /*guardamos el monto de recargo */
		call recuperacionvalores.Calculodevalores(new.CodigoPago1, @resultado);
		set new.MontoRecargo:= @resultado;
		
        /*pasamos a actulizar la tabla pagos en la cual se suma el valor del servicio*/
		
        /*priemro obtenemos el codigo del servicio de la tabla pago*/
		SET codigoser :=(SELECT CodigoServicio5 FROM Pago WHERE CodigoPago= new.CodigoPago1);
		/*Con el el codigo obtenedo del servicio se precede a traer el el valor del servicio*/
		SET valorSer := (SELECT ValorServicio FROM servicio WHERE CodigoServicio = codigoser);
		/*pasamos a sumar los dos valores */
        set montonew := valorSer + new.MontoRecargo;
        /*con la suma del recargo y el valor del servicio se procede a actualizar la tabla pago*/
        update pago set MontoTotal = montonew , Estado = 0 , Justificacion = 'Actualización automatica sumando el monto de recargo del pago total'	
        where CodigoPago = new.CodigoPago1;
    
	END;
$$
DELIMITER ;

DELIMITER $$
CREATE
    TRIGGER `pagorecargoparcial` before insert ON `pagoparcial`
    FOR EACH ROW BEGIN
		call recuperacionvalores.Calculodevalores(new.CodigoPago2, @resultado);
		set new.MontoRecargoParcial:= @resultado;
	END;
$$
DELIMITER ;



DELIMITER $$ 
 
CREATE valor TRIGGER updateValorTotal  
AFTER INSERT 
ON PagoTotal   
FOR EACH ROW BEGIN 

END; 
 
DELIMITER $$




/*vista diaria de los 5 servcios com mayor recaudacion*/
create view vistaserviciodiaria
as 
Select  servicio.NombreServicio,servicio.Descripcion as 'Descripción',servicio.ValorServicio,pago.MontoTotal,comprobante.Fecha 
from servicio inner join pago on servicio.CodigoServicio = pago.CodigoServicio5 inner join
 comprobante on pago.CodigoPago = comprobante.CodigoPago3 and comprobante.Fecha = 
 current_date() ORDER BY pago.MontoTotal DESC LIMIT 5 
;

create view vistaserviciosemanal
as 
Select  servicio.NombreServicio, servicio.Descripcion,servicio.ValorServicio,pago.MontoTotal,comprobante.Fecha
from servicio inner join pago on servicio.CodigoServicio = pago.CodigoServicio5
 inner join comprobante on pago.CodigoPago = comprobante.CodigoPago3 
 and comprobante.Fecha >= DATE_SUB(NOW(),INTERVAL 7 DAY) ORDER BY pago.MontoTotal DESC LIMIT 5 
;
create view vistaempleadosdiaria as 
SELECT DISTINCT empleado.CodigoEmpleado AS 'Código empleado',usuario.Nombre,
usuario.Apellido,usuario.Cedula, (SELECT SUM(comprobante.MontoCancelado)  
AS 'Monto cancelado' FROM pago INNER JOIN comprobante ON pago.CodigoEmpleado2 = empleado.CodigoEmpleado  
AND comprobante.CodigoPago3=pago.CodigoPago and comprobante.Fecha=current_date()
) AS Total
FROM usuario INNER JOIN empleado ON usuario.CodigoUsuario=empleado.CodigoUsuario1
INNER JOIN pago ON pago.CodigoEmpleado2 = empleado.CodigoEmpleado 
INNER JOIN  comprobante ON  pago.CodigoPago = comprobante.CodigoPago3 AND 
comprobante.Fecha = CURRENT_DATE() ORDER BY Total DESC LIMIT 5 ;


/*Vista semanal*/
/*
create view vistaempleadossemanal as 
SELECT DISTINCT empleado.CodigoEmpleado AS 'Código empleado',usuario.Nombre,
usuario.Apellido,usuario.Cedula, (SELECT SUM(comprobante.MontoCancelado)  
AS 'Monto cancelado' FROM pago INNER JOIN comprobante ON pago.CodigoEmpleado2 = empleado.CodigoEmpleado  
AND comprobante.CodigoPago3=pago.CodigoPago and comprobante.Fecha >= DATE_SUB(NOW(),INTERVAL 7 DAY)
) AS Total
FROM usuario INNER JOIN empleado ON usuario.CodigoUsuario=empleado.CodigoUsuario1
INNER JOIN pago ON pago.CodigoEmpleado2 = empleado.CodigoEmpleado 
INNER JOIN  comprobante ON  pago.CodigoPago = comprobante.CodigoPago3 AND 
comprobante.Fecha >= DATE_SUB(NOW(),INTERVAL 7 DAY) ORDER BY Total DESC LIMIT 5 ;
*/

create function fecha1() returns DATE DETERMINISTIC NO SQL return @fecha1;
create function fecha2() returns DATE DETERMINISTIC NO SQL return @fecha2;


create view vistaempleado as 
SELECT DISTINCT empleado.CodigoEmpleado AS 'Código empleado',usuario.Nombre,
usuario.Apellido,usuario.Cedula, (SELECT SUM(comprobante.MontoCancelado)  
AS 'Monto cancelado' FROM pago INNER JOIN comprobante ON pago.CodigoEmpleado2 = empleado.CodigoEmpleado  
AND comprobante.CodigoPago3=pago.CodigoPago and comprobante.Fecha >= fecha1() and comprobante.Fecha <= fecha2()
) AS Total
FROM usuario INNER JOIN empleado ON usuario.CodigoUsuario=empleado.CodigoUsuario1
INNER JOIN pago ON pago.CodigoEmpleado2 = empleado.CodigoEmpleado 
INNER JOIN  comprobante ON  pago.CodigoPago = comprobante.CodigoPago3 AND 
comprobante.Fecha >= fecha1() and comprobante.Fecha <= fecha2() ORDER BY Total
 DESC LIMIT 5 ;


/*BUSQUEDA SEGUN UNA FECHA DEFINIDA*/
#select s.* from (select @fecha1:='2022-09-01' p, @fecha2:='2022-09-09' a) param , vistaempleado s;
/*datos de prueba */
#provincia

insert into provincia(Nombre) 
values ('Loja');
insert into provincia(Nombre) 
values ('Cuenca');

insert into provincia(Nombre) 
values ('Quito');

#institucion

insert into institucion(Nombre,Direccion,Telefono,Correo,Descripcion) 
values('Daniel Alvarez burneo ','Avenida ooriilas de zamora ','07255489','daniel@gamil.com','Institucion de edcucacio primaria');

insert into institucion(Nombre,Direccion,Telefono,Correo,Descripcion) 
values('Liceo americano','Calle faraday','07254533','liceo@gamil.com','Institucion de edcucacio Basica');

#compañia 

insert into `recuperacionvalores`.`compañia` (`Nombre`, `Dirección`, `Telefono`, `Descripcion`, `Correo`, `Logo`, `CodigoProvincia`)
	values
	('UIE', 'AV cubibanba ', '07298514', 'Universidad Internacional del Ecuador', 'uie@uie.edu.ec', 'url@url ', '1');
insert into `recuperacionvalores`.`compañia` (`Nombre`, `Dirección`, `Telefono`, `Descripcion`, `Correo`, `Logo`, `CodigoProvincia`)
	values
	('UNL', 'Av Pio jarramillo ', '07599155', 'Universidad nacinal de loja', 'unl@unl.edu.ec', 'url@url ', '1');

#convenio
insert into `recuperacionvalores`.`convenio` (`FechaInicio`, `Descripcion`, `TipoConvenio`, `FechaFin`, `CodigoCompañia1`, `CodigoInstitucion1`)
values
('2022-09-03', 'relaciones de cobrp\r\n\r\n', 'Largo plazo', '2022-09-03', '1', '1'),
('2022-08-10', 'Cobro y cobinacion de proyectos', 'Madiano plazo', '2023-08-10', '2', '1');
    
#usuario 

insert into `recuperacionvalores`.`usuario` (`Nombre`, `Apellido`, `Cedula`, `Direccion`, `Telefono`, `Correo`, `CodigoCompañia2`)
	values
	('Carlos', 'Alvares', '115059964', 'Mercadillo ', '0987751885', 'avdv@gmmail.com', '1'),
	('Fernanda', 'logan', '1150577825', 'Azuay y 18 de noviembre  ', '0987751485', 'basd@gmmail.com', '1'),
	('Cristobal', 'Rios', '115339756', 'Av primero de mayo ', '0980239009', 'cris@gmmail.com', '1'),
	('Victor', 'Jimenez', '1150599056', 'Mexicali y Valladares ', '0982152997', 'alfredoji300@gmmail.com', '1'),
	('Erika', 'Herrera', '110533975', 'Trapichillo ', '0990606799', 'erika@gmmail.com', '1'),
	('Critina', 'Rios', '1133456758', 'AV primero de mayo esquina', '0967609099', 'alfredoji300@gmmail.com', '1'),
    ('Karolina', 'Martina', '1154988798', 'av sucre ', '09671221099', 'karolina@gmmail.com', '1'),
    ('MArtha', 'Quiñonez', '1133996758', 'Pitas', '0967609022', 'martha@gmmail.com', '1'),
    ('Paula', 'Rios', '1133456722', 'Los molinos', '0967609011', 'paula@gmmail.com', '1'),
    ('Jessenia', 'Vicente', '1133499758', 'San vicente', '0987609099', 'jess@gmmail.com', '1'),
    ('Jessica', 'Tapia', '0933456758', 'Las pitas', '087609099', 'jesstapia@gmmail.com', '1'),
    ('Jennifer', 'Jimenez', '1133456758', 'AV primero de mayo esquina', '0967609099', 'alfredoji300@gmmail.com', '1');
    
  
	# Entidad empleado 
    insert into `recuperacionvalores`.`empleado` (`CodigoIdentificacion`, `HorasTrabajo`, `CodigoUsuario1`,Constraseña)
	values
	('123', '8', '1','123456789'),
	('124', '10', '2','123456789'),
	('125', '8', '5','123456789');
    

#cliente 
insert into `recuperacionvalores`.`cliente` (`RUC`, `CodigoUsuario2`)
	values
	('1150599056001 ', '3');
insert into `recuperacionvalores`.`cliente` (`RUC`, `CodigoUsuario2`)
	values
	('1150599056055 ', '4');
    
    insert into `recuperacionvalores`.`cliente` (`RUC`, `CodigoUsuario2`)
	values
	('1150599056055 ', '6');
    
    #tabla acceso 
insert into `recuperacionvalores`.`acceso` (`Fecha`, `Hora`, `CodigoEmpleado1`)
	values
	('2022-09-01', '20:26', '1');
insert into `recuperacionvalores`.`acceso` (`Fecha`, `Hora`, `CodigoEmpleado1`)
	values
	('2022-09-01', '21:59', '2');
insert into `recuperacionvalores`.`acceso` (`Fecha`, `Hora`, `CodigoEmpleado1`)
	values
	('2022-09-02', '08:45', '3');

#entidad servicio

insert into `recuperacionvalores`.`servicio` (`NombreServicio`, `Descripcion`, `ValorServicio`, `CodigoCliente1`,`C_CodigoCompañia`)
	values
	('Television', 'Tv Cable Paquet basico HD', '34', '1','1');
insert into `recuperacionvalores`.`servicio` (`NombreServicio`, `Descripcion`, `ValorServicio`, `CodigoCliente1`,`C_CodigoCompañia`)
	values
	('Electricidad', 'Pagos de consumos mensuales', '15', '1','1');
insert into `recuperacionvalores`.`servicio` (`NombreServicio`, `Descripcion`, `ValorServicio`, `CodigoCliente1`,`C_CodigoCompañia`)
	values
	('Prestamo bancario', 'Pago de mensualidad de prestamo comun', '35', '2','1');
insert into `recuperacionvalores`.`servicio` (`NombreServicio`, `Descripcion`, `ValorServicio`, `CodigoCliente1`,`C_CodigoCompañia`)
	values
	('Agua potable', 'Distribucion zonal norte', '12', '2','1');
insert into `recuperacionvalores`.`servicio` (`NombreServicio`, `Descripcion`, `ValorServicio`, `CodigoCliente1`,`C_CodigoCompañia`)
	values
	('Telefonia fija', 'Compañia Telconet', '15', '2','1');
insert into `recuperacionvalores`.`servicio` (`NombreServicio`, `Descripcion`, `ValorServicio`, `CodigoCliente1`,`C_CodigoCompañia`)
	values
	('Television tv cable', 'Tv Cable Paquet basico HD', '34', '2','1');
insert into `recuperacionvalores`.`servicio` (`NombreServicio`, `Descripcion`, `ValorServicio`, `CodigoCliente1`,`C_CodigoCompañia`)
	values
	('Electricidad ', 'Pago de cosumo mensules ', '34', '2','1');
insert into `recuperacionvalores`.`servicio` (`NombreServicio`, `Descripcion`, `ValorServicio`, `CodigoCliente1`,`C_CodigoCompañia`)
	values
	('Colegiatura mixta', 'PAgo de mesulidades del colegio Isidro', '75', '3','1');

    
insert into prestamo(FechaLimite,TipoPrestamo,CodigoServicio1)
values 
('15','Frances',3)
;    
    
insert into serviciobasico(Tipo)
values('Energia Electrica');    
insert into serviciobasico(Tipo)
values('Agua');    
insert into serviciobasico(Tipo)
values('Telefonia');    

    
 insert into registroserviciob(CodigoSBasico1,CodigoServicio6) 
 values('1','2');
 
 insert into registroserviciob(CodigoSBasico1,CodigoServicio6) 
 values('2','4');
 
 
 insert into registroserviciob(CodigoSBasico1,CodigoServicio6) 
 values('3','5');
 
 insert into registroserviciob(CodigoSBasico1,CodigoServicio6) 
 values('3','7');
    
    #Television satelital
    insert into televisionsatelital(NroDispositivos,Tipo,CodigoServicio3)
    values('3','Television HD','1'),
    ('2','Television ','6');
    
    
    #colegiatura 
    insert into colegiatura(Año,NombreEstudiante,ApellidoEstudiante,Correo,InstitucionEducativa,CodigoServicio4)
    values('2019','Carlos ','Ordoñes','Carlos@gmail.com','COlegio Isidro ROmero','6');
    
#Entidad Pago
/*
 insert into  pago(Fecha,FechaLimite,MontoTotal,NroPago,Estado,CodigoEmpleado2,CodigoServicio5) 
 values('2022-09-01','2022-09-02','41','1','1','1','1'),
 ('2022-09-02','2022-09-03','17.50','2','1','1','3'),
 ('2022-09-03','2022-09-05','35.50','3','1','1','8'),
 ('2022-09-04','2022-09-06','35.50','3','1','1','2');
*/
 insert into  pago(Fecha,FechaLimite,MontoTotal,NroPago,Estado,CodigoEmpleado2,CodigoServicio5) 
 values('2022-09-01','2022-09-02','41','1','1','1','1'),
 ('2022-09-02','2022-09-03','17.50','2','1','1','3'),
 ('2022-09-03','2022-09-05','35.50','3','1','1','8'),
 ('2022-09-04','2022-09-06','35.50','3','1','1','2');
    
    insert into	comprobante(MontoCancelado,Fecha,Estado,CodigoPago3)
    values ('41.00','2022-09-01',0,'1'),
    ('17.50','2022-09-01',0,'2');
    
    
    #Entidad pagototal
    
    insert into pagototal(CodigoPago1)
    values ('1'),
    ('2');
    
    
    #pago parcial
    
    #CodigoPagoPArcial
    
    insert into pagoparcial(MontoRecargoParcial,PorcentajePago,NroCuotas,CodigoPago2)
    values('1','50','2','3');
     insert into pagoparcial(MontoRecargoParcial,PorcentajePago,NroCuotas,CodigoPago2)
    values('0.15','75','2','4');
      #Couta
    insert into cuota(NroCuota,NroCuotasPagadas,ValorCuota,ValorTotal,Estado,CodigoPagoP1)
    values 
    ('1','1','17.75','17.75','1','1'),
    ('2','2','17.75','0','1','1');
    
    